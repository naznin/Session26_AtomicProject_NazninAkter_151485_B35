<?php
namespace App\Gender;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Gender extends DB{
    public $id="";
    public $person_name="";
    public $gender="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('gender',$postVariableData)){
            $this->gender = $postVariableData['gender'];
        }
    }



    public function store(){

    $arrData = array( $this->person_name, $this->gender);

    $sql = "Insert INTO gender(person_name, gender) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method







    public function index(){
        echo "I am inside index method of <b>BookTitle</b> Class";
    }




}

?>

